As per Chris's excellent suggestion, this is a casual email describing
Datashards stores. This document, due to its size, is something in between a
casual email and a spec.

* Overview

Datashards is a system where data is stored in such a way to make it possible
to reason about encrypted data objects and share them between computers safely.
Datashards consists of two parts, a "store" and a "client". The store is
entirely focused on the storage and retrival of data, but remains unaware of
the contents of the shards themselves. The client is responsible for talking to
a store and presenting it with encrypted data. This email discusses the store
component of Datashards.

* Store Overview

In its most basic form, a store is nothing more than an association of
key/value pairs, where the key is the shard identifier (the XT) and the value
is the encrypted data of the shard. In fact, it's possible to implement a
Datashards store's backend using a simple hash table or other key/value data
structure.

* Store Descriptors

There is one other thing that a store needs, and that is a Store Descriptor. A
store descriptor is a document that the store gives to the client that
describes its algorithmic suites, and its methods. This is because not ever
store will offer every suite, and not every store will offer every method to
the clients.

Moreover it may be possible that a specific client only gets access to specific
methods, or specific methods in relation to one suite but not others. That's
why the descriptor is so important.

NOTE: In the future, the descriptor, or the methods, may need to specify things
that are not yet set, like the number of request they accept, like "gets", ie a
"gets 4", "gets 16", etc. It may be worthwhile to consider something more
sophisticated in terms of describing these methods than a list.

* Suites

Suites are essentially two things- the hashing algorithm used and the size of
the shards themselves. Right now we only support one suite, sha256d-32k, though
we may support others in the future. The first part of the suite is the hashing
algorithm- in this case sha256d. The next part is the size of the chunk, which
is 32k, or 32768 bytes.

That means all chunks must be exactly 32k in length. As an aside, knowing the
exact size of the shard beforehand means that a store author could potentially
use this knowledge to make indexing the data faster.

The hashing algorithm specified means that if we take the data in the specified
shard and run the algorithm over it, we should get back the same digest.

* Store Methods

Now that we've covered the suite, let's talk about the methods. Aside from the
descriptor, no actual methods are required. It might be that the descriptor
doesn't offer any, especially to a specific client. Common methods are "get",
"put", "catalog" and "delete".

The names of the methods should be uniform across implementations and we
suggest (though don't require) that these same method names be used in any
client side applications.

NOTE: I'm not including any remote methods in this document, including
asynchronous requests. We're keeping this document simple for now.

*** get (xt)

The ~get~ method instructs the store to retrieve the requested shard (xt). If
the request fails to return the requested XT, an empty value should be
returned.

** gets (xts)

The ~gets~ method instructs the store to retrieve the requested shards (xts).
The results of this request must follow in sequence to the original request.

** put (data)

The ~put~ method instructs the store to save the request data. The return
result of this call should be the XT of the newly stored data.

** puts (data, data)

The ~puts~ method instructs the store to save the requested data sequences. The
return result of this call should be a sequence XTs of the request in the same
order that they were submitted.

** catalog

The ~catalog~ method instructs the store to return a list of XTs to it.

** delete

The ~delete~ method instructs the store to delete the shard specified by the XT
passed to it. It should return the XT of the deleted shard if the shard was
present and removed.

** deletes

The ~deletes~ method instructs the store to delete the specified shards by XT.
It should return a sequence of XTs that were removed, preferably in the order
of the original request.

* Store Descriptor Format

The store descriptor document is sent to the client in the Store Descriptor
Format, which is a cannonical s-epxression document connsisting of a tree of
values, the first is the suite, and under each suite are the methods available
to that suite.

An example of this document, in s-expresion form, would look something like:

(suite (method1 method2))

In practice, this would look more like:

(sha256d ( (get "https://example.org/")
           (put "https://example.org")
           (catalog "https://example.org/catalog")))

There are a few things to notice here. The first is that the we aren't
specifying any kind of store identificator in the descriptor. That's a design
choice around the fact that some stores may be either accessible from multiple
routes or may be passive and unware of their location.

The second thing to notice is that we have those URLs in after the methods.
That's to indicate, where appropriate, where each method may be operating. The
implementation for a store's endpoint will depend on its implementation of the
store itself. We specify a URL for each of the methods, though this URL may be
optionally relative to the store's URL. If a URL doesn't make sense (eg for a
memory store) then either a blank url, "" is recommended. In cases where an
authorization token is required to access the URL, then that URL should be
expressed in bearcaps.

* Store Types and URL Construction

Stores can vary wildly in implementation. For example a store's backend may be
as simple as a hash table, or even just a directory on a filesystem, or they
may be very complex and able to operate on a network and communicate with other
stores. We want to have the flexibility for store authors to create systems
that make sense for them, while at the same time make it easy for client
library writers to keep interfaces uniform across stores.

To do that, we use URLs to reference stores.


* URLs, and URLs, and URIs

There are so many different uses of URLs and URIs in Datashards that it can be
confusing. Here's a handy table of the different types of URLs/URIs and their
uses:

| Names        | URN Type      | Description                                                |
|--------------+---------------+------------------------------------------------------------|
| Store        | Bearcap URL   | An store in a location                                     |
| Store Method | Bearcap URL   | A store method in a location                               |
| Datashard    | URI           | Information about the Datashard (has an XT andpossibly AS) |
| XT           | URI           | A specific shard w/ suite information                      |
| AS           | Store URLs    | An initial store to find a shard                           |

Datashards urls and their construction will be described in the sister document
to this one describing Datashards clients. The only thing we need to
specifically mention is that XTs are a form of URI and we will use XTs to
get/put specific shards from the store.

* Representing Stores

Just as Datashards provides a mechanism to allow us to discuss specific shards,
it also provides a mechanism to discuss stores. This could be used by either
clients who wish to connect to the store, or by stores connecting to other
stores.

Stores are represented as URIs, sURLs. In the URLs, the store type is listed as
the protocol, eg magenc://. There is no need to explicitly mention the
underlying transport protocol in most cases because the store type itself will
be tied to a specific transport protocol.

Where that isn't the case, we can add a + and specify the transport protocol
after the store type, ala magenc+http. In this case we are explicitly
telling the client application to use http as the transport protocol, but this
should be the exception rather than the rule. Generally a store type will have
a single (default) transport protocol.

When sending a store alongside an authorization token, the bearcap method of
URL construction should be employed.

* Store Types

Currently Datashards supports three types of stores, with more types supported
soon.

** memory

A memory store is a store primarily used for testing in which there
is an expecation that data will reside in transient memory.

** simplefile

The simplefile store is a store in which data is stored by filename according
to the following structure:

/
/description - the Store desccriptor document
/<suite>/<digest> - the XT's contents

Due to underlying filesystem limitations, this store may be limited to the
maximum number of files per directory, eg 32,000 on ext2/3 or 64,000 on FAT,
rendering the maximum number of files to 1tb or 2tb respectively.

We also assume no other restrictions on files, such as 8 character file
limitations such as were present in FAT16.

** doozer

A doozer store is a remote store accessed over https (by default) or http. The
URL form of a doozer store is thus ~doozer://<host><opional port><resource>~,
eg ~doozer://example.com/ds~, where an http request would be
~doozer+http://example.com/ds~.

NOTE: This used to be called fizzgig. I'm also removing the "remote" label from
the software. This makes it clearer to clients and will soon be reflected in
the software as I move pydatashards to something indicating it's a client
library.



NOTE: Should we also standardize errors/exceptions from software?
